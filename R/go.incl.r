# functions for testing enrichment
# of go terms in gene lists
# written by denes (turei.denes@gmail.com)

library("ALL")
library("hgu95av2.db")
library("GO.db")
library("annotate")
library("genefilter")
library("GOstats")
library("RColorBrewer")
library("xtable")
library("Rgraphviz")
library("org.Hs.eg.db")
library("GOSemSim")
library("digest")

# this set of functions is available here:
# http://faculty.ucr.edu/~tgirke/Documents/R_BioCond/My_R_Scripts/GOHyperGAll.txt
source("gohypergall.r")

init.go.cache <- function(){	
	if(!exists("cacheFile")){
		cacheFile <<- paste(rndstr(5),".goss.cache",sep="")
		# cacheLog(paste("new temporary cache file: ",cacheFile,sep=""))
	}
	lockStr <- rndstr(5)
	# cacheLog(paste("trying to lock: ",lockStr,sep=""))
	while(TRUE){
		if(file.exists("goss.cache.lock")){
			# cacheLog("already locked, waiting...")
			Sys.sleep(1)
			next
		}else{
			write(lockStr,file="goss.cache.lock")
			lck <- as.character(read.table("goss.cache.lock",colClasses=c("character")))
			if(lck!=lockStr){
				# cacheLog("interfering locks, waiting...")
				Sys.sleep(1)
				next
			}else{
				# cacheLog("successfully locked!")
				if(file.exists(cacheFile)){
					# cacheLog(paste("appending to main: ",cacheFile,sep=""))
					system(paste("cat ",cacheFile," >> goss.cache",sep=""),wait=TRUE)
					file.remove(cacheFile)
					# cacheLog(paste("removed: ",cacheFile,sep=""))
				}
				# cacheLog(paste("newly created: ",cacheFile,sep=""))
				file.create(cacheFile)
				if(file.exists("goss.cache")){
					# cacheLog("reading from main")
					ss.cache <<- unique(read.table("goss.cache",sep="\t",header=FALSE))
					# cacheLog(paste("writing unique main, rows: ",dim(ss.cache)[1],sep=""))
					# write.table(ss.cache,  file="goss.cache", sep="\t", 
					#		col.names=FALSE, quote=FALSE, row.names=FALSE)
				}else{
					ss.cache <<- NULL
				}
				# cacheLog("removing lock")
				file.remove("goss.cache.lock")
				return()
			}
		}
	}
}

init.cache <- function(cache="goss") {
	cacheFileName <- paste(cache,"CacheFile",sep="")
	mainCacheFile <- paste(cache,".cache",sep="")
	cacheLockFile <- paste(cache,".cache.lock",sep="")
	lockFile <- paste(cache,".cache.lock",sep="")
	cacheVarName <- paste(cache,".cache",sep="")
	if(!exists(cacheFileName)){
		assign(cacheFileName,paste(rndstr(5),".",cache,".cache",sep=""),envir=.GlobalEnv)
		# cacheLog(paste("new temporary cache file: ",cacheFile,sep=""))
	}
	tmpCacheFile <- get(cacheFileName)
	lockStr <- rndstr(5)
	# cacheLog(paste("trying to lock: ",lockStr,sep=""))
	while(TRUE){
		if(file.exists(lockFile)){
			# cacheLog("already locked, waiting...")
			Sys.sleep(1)
			next
		}else{
			write(lockStr,file=lockFile)
			lck <- as.character(read.table(lockFile,colClasses=c("character")))
			if(lck!=lockStr){
				# cacheLog("interfering locks, waiting...")
				Sys.sleep(1)
				next
			}else{
				# cacheLog("successfully locked!")
				if(file.exists(tmpCacheFile)){
					# cacheLog(paste("appending to main: ",cacheFile,sep=""))
					system(paste("cat ",tmpCacheFile," >> ",mainCacheFile,sep=""),wait=TRUE)
					file.remove(tmpCacheFile)
					# cacheLog(paste("removed: ",cacheFile,sep=""))
				}
				# cacheLog(paste("newly created: ",cacheFile,sep=""))
				file.create(tmpCacheFile)
				if(file.exists(mainCacheFile)){
					# cacheLog("reading from main")
					assign(cacheVarName,
						unique(read.table(mainCacheFile,sep="\t",header=FALSE)),
						envir=.GlobalEnv)
					# cacheLog(paste("writing unique main, rows: ",dim(ss.cache)[1],sep=""))
					# write.table(ss.cache,  file="goss.cache", sep="\t", 
					#		col.names=FALSE, quote=FALSE, row.names=FALSE)
				}else{
					assign(cacheVarName,
						NULL,
						envir=.GlobalEnv)
				}
				# cacheLog("removing lock")
				file.remove(lockFile)
				return()
			}
		}
	}
}

cacheLog <- function(str){
	write(paste(" ::: ",method," :: ",str,sep=""),file="cache.log",append=TRUE)
	return()
}

procLog <- function(str){
	write(paste(" ::: ",date()," ::: ",method," :: ",str,sep=""),file="proc.log",append=TRUE)
	return()
}

totalgoSSTime <<- NULL
totalgoSSCompTime <<- NULL

init.cache("goss")
init.cache("ppi")
init.cache("mod")

annot2list <- function(a,d){
	lst <- list()
	for(g in d[,1]){
		lst[[g]] <- character(0)
	}
	for(i in 1:dim(a)[1]){
		lst[[a[i,1]]] <- c(lst[[a[i,1]]],as.character(a[i,2]))
	}
	return(lst)
}

top.go <- function(g,BP_node_affy_list,GO_BP_DF,ntop=20){
	# bp_annot_list <- annot2list(annot.univ,bp)
	# BP_node_affy_list <- bp_annot_list
	# GO_BP_DF <- annot.univ
	go.top <- GOHyperGAll(gocat="BP",sample=g,Nannot=2)
	go.top <- go.top[1:ntop,]
	return(go.top)
}

go.enrichment <- function(c,G){
	# reading go---gene annotations
	# annot.universe <- "inter_tftfbs_goannot.csv"
	# iterate modules
	t <- list()
	for(l in levels(as.factor(c$membership))){
		g <- comm.members(c,l)
		go.top <- top.go(g,G$bp_annot_list,G$annot.univ,20)
		t[[l]] <- go.top
	}
	return(t)
}

go.read.data <- function(G){
		write(paste("  == ",date(),
			" Reading GO annotation universe",".",sep=""),file=G$logfile,append=TRUE)
		file.annot.univ <- paste(G$input,"_goannot.csv",sep="")	
		G$annot.univ <- read.table(file=file.annot.univ,sep="\t",header=FALSE)
		G$annot.univ <- as.data.frame(cbind(as.character(G$annot.univ[,2]),as.character(G$annot.univ[,1])))
		colnames(G$annot.univ) <- c("GOID","GeneID")
		write(paste("  == ",date(),
			" Reading BP domain",".",sep=""),file=G$logfile,append=TRUE)
		# reading list of all GOs in BP domain
		G$bp <- read.csv("bp_go.csv",header=FALSE)
		# generating list format
		write(paste("  == ",date(),
			" Generating list format",".",sep=""),file=G$logfile,append=TRUE)
		G$bp_annot_list <- annot2list(G$annot.univ,G$bp)
		BP_node_affy_list <<- G$bp_annot_list
		GO_BP_DF <<- G$annot.univ
		return(G)
}


load.go.packages <- function(){
	detach("package:igraph",unload=TRUE)
	library("ALL")
	library("hgu95av2.db")
	library("GO.db")
	library("annotate")
	library("genefilter")
	library("GOstats")
	library("RColorBrewer")
	library("xtable")
	library("Rgraphviz")
}

unload.go.packages <- function(){
	detach("package:Rgraphviz",unload=TRUE)
	detach("package:GOstats",unload=TRUE)
	detach("package:annotate",unload=TRUE)
	detach("package:xtable",unload=TRUE)
	detach("package:RColorBrewer",unload=TRUE)
	detach("package:genefilter",unload=TRUE)
	detach("package:GO.db",unload=TRUE)
	detach("package:hgu95av2.db",unload=TRUE)
	detach("package:ALL",unload=TRUE)
	library("igraph")
}

uniprot2GO <- function(uniprot){
	entrez <- mget(uniprot, revmap(org.Hs.egUNIPROT))
	gos <- lapply(entrez,get,org.Hs.egGO)
	golst <- list()
	MF <- BP <- CC <- NULL
	for(goo in names(gos)){
		for(go in names(gos[[goo]])){
			ont <- gos[[goo]][[go]]$Ontology
			goid <- gos[[goo]][[go]]$GOID
			assign(ont,c(get(ont),goid))
		}
		golst[[goo]][["BP"]] <- BP
		#golst[[goo]][["CC"]] <- CC
		#golst[[goo]][["MF"]] <- MF
		MF <- BP <- CC <- NULL
	}
	return(golst)
}

uniprot2tgtGO <- function(uniprot){
	golst <- list()
	golst[[uniprot]][["BP"]] <- as.vector(GO_BP_DF[GO_BP_DF[,2]==uniprot,1])
	# golst[[uniprot]][["CC"]] <- golst[[uniprot]][["MF"]] <- NULL
	return(golst)
}

TFmodule <- function(tf,module){
	#print(paste("tf",tf))
	tfgo <- uniprot2GO(tf)[[tf]][["BP"]]
	#print(tfgo)
	sss <- NULL
	for(m in module){
		#print("getting go list for")
		#print(m)
		procLog(paste("»»» module member: ",m,sep=""))
		mgo <- uniprot2tgtGO(m)[[m]][["BP"]]
		procLog(paste("»»» go pairs: ",length(tfgo)*length(mgo),sep=""))
		#print(mgo)
		#print("calculating ss")
		# ss <- mgoSim(tfgo, mgo, ont="BP", measure="Wang", combine="avg")
		ss <- as.numeric(ppi.cache[ppi.cache[,1]==m & ppi.cache[,2]==tf,3])
		if(length(ss)==0){
			ss <- mgoss(tfgo,mgo)
			ppi.cache <<- rbind(ppi.cache,m,tf,ss)
			write(paste(m,"\t",tf,"\t",ss,sep=""),file=ppiCacheFile,append=TRUE)
			init.cache("ppi")
		}
		sss <- c(sss,ss)
		#print("..")
	}
	return(mean(sss,na.rm=TRUE))
}

getFunctionalSim <- function(G,c){
	s <- NULL
	procLog(paste("number of modules: ",length(levels(as.factor(c$membership))),sep=""))
	for(cc in levels(as.factor(c$membership))){
		module <- V(G)$id[V(G)[c$membership==as.numeric(cc)]]
		procLog(paste("» starting module #",cc,", members: ",length(module),sep=""))
		procLog(paste("» number of pairs: ",length(module)*(length(module)-1),sep=""))
		#print("M")
		for(tf in module){
			procLog(paste("»» new tf: ",tf,sep=""))
			mod.hash <- digest(paste(sort(module[module!=tf]),collapse=""),algo="md5")
			sss <- as.numeric(mod.cache[mod.cache[,1]==mod.hash & mod.cache[,2]==tf,3])
			if(length(sss)==0){
				sss <- TFmodule(tf,module[module!=tf])
				mod.cache <<- rbind(mod.cache,mod.hash,tf,sss)
				write(paste(mod.hash,"\t",tf,"\t",sss,sep=""),file=modCacheFile,append=TRUE)
				init.cache("mod")
			}
			s[tf] <- sss
			#print("T")
		}
	}
	return(s[V(G)$id])
}

mgoss <- function(goOne,goTwo){
	sss <- NULL
	cat("\t",file="proc.log",append=TRUE)
	for(goO in goOne){
		for(goT in goTwo){
			cat(".",file="proc.log",append=TRUE)
			tot <- proc.time()
			# print(paste("from module: ",goT,"from tf: ",goO,sep=""))
			goo <- sort(c(goO,goT))
			lkp <- proc.time()
			ss <- as.numeric(goss.cache[goss.cache[,1]==goo[1]&goss.cache[,2]==goo[2],3])
			lkp <- proc.time() - lkp
			cacheLog(paste("array lookup time: ",lkp,sep=""))
			if(length(ss)==0){
				# print(paste('ss <- goSim(',goO,',',goT,', ont="BP", measure="Wang")',sep=""))
				#ptm <- proc.time()
				ss <- goSim(goo[1], goo[2], ont="BP", measure="Wang")
				#ptm <- proc.time() - ptm
				# print(paste("result: ",ss,sep=""))
				#wtm <- proc.time()
				if(!is.na(ss)){
					goss.cache <<- rbind(goss.cache,c(goo[1],goo[2],ss))
					write(paste(goo[1],"\t",goo[2],"\t",ss,sep=""),file=gossCacheFile,append=TRUE)
				}
				#wtm <- proc.time() - wtm
				#cat(paste("Computed: ",goO,goT,ss,"\n",sep="\t"))
				#ptm <- ptm["user.self"]+ptm["sys.self"]
				#cat(paste("Computation time: ",ptm,"\n",sep="\t"))
				#totalgoSSCompTime <<- c(totalgoSSCompTime,ptm)
				#cat(paste("Array insert & file write time: ",wtm["user.self"]+wtm["sys.self"],"\n",sep="\t"))
			}
			sss <- c(sss,ss)
			#else{
				#cat(paste("From cache: ",goO,"\t",goT,"\t",ss,"\n",sep=""))
			#}
			#cat(paste("Array lookup time: ",lkp["user.self"]+lkp["sys.self"],"\n",sep="\t"))
			#sss <- c(sss,ss)
			#tot <- proc.time() - tot
			#tot <- tot["user.self"]+tot["sys.self"]
			#cat(paste("Total time: ",tot,"\n",sep="\t"))
			#totalgoSSTime <<- c(totalgoSSTime,tot)
			#write(paste(
			#	"Total Average:\t",mean(totalgoSSTime),"\n",
			#	"Total Computation Average:\t",mean(totalgoSSCompTime),
			#	sep=""),file="goss.stats",append=FALSE)
		}
		init.cache("goss")
	}
	return(mean(sss,na.rm=TRUE))
}

