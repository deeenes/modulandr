### this file contains wrapper functions to execute external commands
### and check their output

# check the output of external process
extcomm.ready <- function(commname="External command",status=0){
	if(length(status)==0){
		status <- 0
	}
	if(status==0){
		cat(paste(" :: ",commname," exited with status 0\n",sep=""))
	}else{
		cat(paste(" :: ",commname," exited with non zero status\n>>> stop\n",sep=""))
		stop()
	}
}

# run an external command
extcomm.run <- function(comm){
	cat(paste(" :: Running ",comm,"\n",sep=""))
	sysout<-system(comm, intern=FALSE, wait=TRUE )
	return(sysout)
}
