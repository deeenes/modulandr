### this is an R interface for Aaron McDaid's 
### clique percolation implementation
### cp5
### http://github.com/aaronmcdaid/MaximalCliques
### it needs 'exec.incl.r' to be loaded
### cp5 binary needs to be installed in the system path

### cp5 clique percolation
# k is the minimum clicque size
cp5.community <- function(G,k=3,keep=FALSE){
	inputfile <- "cp5.input.tmp"
	tmpdir <- "cp5.dir.tmp"
	cp5comm <- paste("cp5 ",inputfile," ",tmpdir," --k=",k,' --stringIDs | grep Written | awk \'{printf $2 "\\t" $7 "\\n"}\' > cp5.cnum.tmp',sep="")
	write.graph(G,inputfile,format="ncol")
	extcomm.run(paste("mkdir ",tmpdir,sep=""))
	sysout <- extcomm.run(cp5comm)
	extcomm.ready("cp5",sysout)
	cnum <- read.csv("cp5.cnum.tmp",sep="\t",header=FALSE)
	maxc <- max(cnum[,1])
	#for(x in 1:dim(cnum)[1]){
	#	if(cnum[x,1]==maxc-1){
	#		 res <- read.csv(paste(tmpdir,"/comm",cnum[x,2],sep=""),sep=" ",header=FALSE)
	#	}
	#}
	res <- read.csv(paste(tmpdir,"/comm",maxc,sep=""),sep=" ",header=FALSE)
	r <- NULL
	for(n in V(G)$name){
		for(i in 1:dim(res)[1]){
			for(j in 1:dim(res)[2]){
				if(n==res[i,j]){
					r <- c(r,i)
				}
				# cat(paste(i,j,n,res[i,j],"\n"))
			}
		}
	}
	if(!keep){
		extcomm.run(paste("rm ",inputfile,sep=""))
		extcomm.run(paste("rm ","cp5.cnum.tmp",sep=""))
		extcomm.run(paste("rm -rf ",tmpdir,sep=""))
	}
	result <- list()
	result$membership <- r
	result$names <- V(G)$name
	result$cnum <- cnum
	result$res <- res
	return(result)
}
