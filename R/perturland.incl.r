### this file contains an R interface for moduland community discovering method family
### it needs 'exec.incl.r' to be loaded
### moduland program binaries need to be installed in the system path

# generic function for all moduland methods
moduland.community <- function(lsfname,hmfname,logfile,
				 landscape="linkland",
				 hill="total",pland_x=0.1,
				 recalculate=FALSE){
	pajek <- paste(lsfname,".net",sep="")
	cxg <- paste(lsfname,".cxg",sep="")
	cxl <- paste(lsfname,".cxl",sep="")
	cxb <- paste(hmfname,".cxb",sep="")
	hmfiles <- paste(lsfname,".cxg ",lsfname,".cxl ",hmfname,".cxb",sep="")
	modfile <- paste(hmfname,".mod",sep="")
	lscomm<- switch(landscape, linkland=paste("linkland -0",cxg,cxl,sep=" "),
			nodeland=paste("nodeland -0",cxg,cxl,sep=" "),
			perturland=paste("perturland",cxg,pland_x,"0",cxl,"0",sep=" "),
			edgeweight=paste("edgeweight --in",cxg,"--out",cxl,sep=" ")
			)
	# cat(paste(landscape,"\n"))
	hmcomm <- switch(hill, total=paste("total_lowmem --no-edge-belong -m 1000 ",hmfiles,sep=""),
			prop=paste("prop ",hmfiles," H --noEdgeBelong",sep=""),
			gradient=paste("prop ",hmfiles," H --noEdgeBelong --gradientMethod",sep="")
			)			
	cat(paste(" :: Input file is ",lsfname,"\n",sep=""))
	if(recalculate || !file.exists(modfile)){
		if(!file.exists(cxg) || !file.exists(cxl) || recalculate){
			if(!file.exists(cxg) || recalculate){
				write(paste("    »» ",date(),
					" Doing ",landscape," ",hill," from pajek format.",sep=""), 
					file=logfile, append=TRUE)
				sysout<-extcomm.run(paste("pajek_conv ",pajek,cxg,sep=" "))
				# cat(paste("sysout=",sysout,"\n",sep=""))
				extcomm.ready("pajek_conv",sysout)
			}else{
				write(paste("    »» ",date(),
					" Doing ",landscape," ",hill," from landscape.",sep=""), 
					file=logfile, append=TRUE)
			}
			sysout<-extcomm.run(lscomm)
			extcomm.ready("perturland",sysout)
		}else{
			write(paste("    »» ",date(),
				" Doing ",landscape," ",hill," from hill method.",sep=""), 
				file=logfile, append=TRUE)
		}
			cat(" :: Executing hill method:\n")
			sysout <- extcomm.run(hmcomm)
			extcomm.ready(status=sysout)
		modext <- paste("cpxext_cat ",hmfname,".cxb | mm.sh > ",modfile,sep="")
		cat(" :: Extracting modules...\n")
		sysout <- extcomm.run(modext)
		extcomm.ready("cpxext_cat",sysout)
	}else{
		write(paste("    »» ",date(),
			" Reading ",landscape," ",hill," data.",sep=""), 
			file=logfile, append=TRUE)
	}
	mod <- read.csv(modfile,sep=",",dec=".",header=FALSE)
	n <- dim(mod)[2]
	m <- dim(mod)[1]
	memb <- NULL
	for(i in seq(1:n)){
		aff <- 0
		md <- 0
		for(j in seq(1:m)){
			if(mod[j,i] > aff){
					aff <- mod[j,i]
					md <- j
			}
		}
		memb <- c(memb,md)
	}
	result <- list()
	result$affinities <- mod
	if(min(memb) == 0){
		memb <- memb + 1
	}
	result$membership <- memb
	result$names <- V(G)$name
	return(result)
}

# wrapper for linkland method
linkland.community <- function(fname,hill="total"){
	res <- moduland.community(fname,landscape="linkland",hill=hill)
	return(res)
}

# wrapper for nodeland method
nodeland.community <- function(fname,hill="total"){
	res <- moduland.community(fname,landscape="nodeland",hill=hill)
	return(res)
}

# wrapper for perturland method
perturland.community <- function(fname,hill="total",x=0.1){
	res <- moduland.community(fname,landscape="perturland",hill=hill,pland_x=x)
	return(res)
}

# wrapper for edgeweight method
edgeweight.community <- function(fname,hill="total"){
	res <- moduland.community(fname,landscape="edgeweight",hill=hill)
	return(res)
}
