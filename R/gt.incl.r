### this file contains functions to use
### graph-tool python library's 
### community functions from R
### graph-tool needs to be installed
### as a package of python 2.7

# source("exec.incl.r")

gt.community <- function(G,nspins=40){
	tmpfile <- "gt.tmp.xml"
	write.graph(G,tmpfile,format="graphml")
	py <- paste( 'echo -en "import sys, os\nfrom pylab import *\nfrom numpy.random import *\nseed(42)\n\nfrom graph_tool.all import *\nfrom graph_tool.community import *\ng = load_graph(\'',tmpfile,'\')\nspins = community_structure(g, 10000, ',nspins,',weight=g.edge_properties[\'weight\'])\ng.vertex_properties[\'spins\'] = spins\ng.save(\'',tmpfile,'\')\n"', ' | python2.7', sep="")
	sysout <- extcomm.run(py)
	extcomm.ready("python2 graph_tool",sysout)
	G <- read.graph(tmpfile,format="graphml")
	result <- list()
	V(G)$spins <- V(G)$spins + 1
	result$membership <- V(G)$spins
	result$names <- V(G)$name
	return(result)
}
