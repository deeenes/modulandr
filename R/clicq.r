
# from http://igraph.wikidot.com/community-detection-in-r

# clicque percolation
clique.community <- function(graph, k) {
   clq <- cliques(graph, min=k, max=k)
   edges <- c()
   for (i in seq_along(clq)) {
     for (j in seq_along(clq)) {
       if ( length(unique(c(clq[[i]], clq[[j]]))) == k+1 ) {
         edges <- c(edges, c(i,j)-1)
       }
     }
   }
   clq.graph <- simplify(graph(edges))
   V(clq.graph)$name <- seq_len(vcount(clq.graph))
   comps <- decompose.graph(clq.graph)

   lapply(comps, function(x) {
     unique(unlist(clq[ V(x)$name ]))
   })
}

# label propagation
lpa.community <- function(g,mode="all"){
  V(g)$group <- as.character(V(g))
  thisOrder <- sample(vcount(g),vcount(g))-1
  t <- 0
  done <- FALSE
  while(!done){
    t <- t+1
    cat("\rtick:",t)
    done <- TRUE ## change to FALSE whenever a node changes groups              
    for(i in thisOrder){
      ## get the neighbor group frequencies:                                    
      groupFreq <- table(V(g)[nei(i,mode=mode)]$group)
      ## pick one of the most frequent:                                         
      newGroup <- sample(names(groupFreq) [groupFreq==max(groupFreq)],1)
      if(done){done <- newGroup==V(g)[i]$group}
      V(g)[i]$group <- newGroup
    }
  }
  ## now fix any distinct groups with same labels:                              
  for(i in unique(V(g)$group)){
    ## only bother for connected groups                                         
    if(!is.connected(subgraph(g,V(g)[group==i]))){
      theseNodes <- V(g)[group==i]
      theseClusters <- clusters(subgraph(g,theseNodes))
      ## iterate through the clusters and append their names                    
      for(j in unique(theseClusters$membership)){
        V(g)[theseNodes[theseClusters$membership==j]]$group <- paste(i,j,sep=".")
      }
    }
  }
  return(g)
}

lpa.fast.community <- function(g, mode="all") {
    vgroups <- V(g)$name
    # random order in which vertices will be processed
    order <- sample(vcount(g), vcount(g))
    t <- 0
    done <- FALSE

    while (!done) {
        t <- t + 1
        ## change to FALSE whenever a node changes groups
        done <- TRUE
        for(i in order) {
            ## get the neighbor group frequencies:
            group.freq <- table(vgroups[neighbors(g, i, mode = mode)])
            ## pick one of the most frequent:
            new.group <- sample(names(group.freq)[group.freq == max(group.freq)], 1)
            if (done) {
                ## we are only done if new group is the same as old group
                done <- (new.group == vgroups[i])
            }
            vgroups[i] <- new.group
        }
    }

    comms <- list(membership = as.numeric(vgroups),
        vcount = vcount(g),
        algorithm = "LPA",
        names = V(g)$name)
    class(comms) <- "communities"
    return(comms)
}
