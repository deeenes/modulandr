### these functions are wrappers for calculating significance of communities
### discovered by any igraph compliant method

### community significance test
community.significance.test <- function(graph, vs, ...) {
    if (is.directed(graph)) stop("This method requires an undirected graph")
    subgraph <- induced.subgraph(graph, vs)
    in.degrees <- igraph::degree(subgraph)
    out.degrees <- igraph::degree(graph, vs) - in.degrees
    wilcox.test(in.degrees, out.degrees, ...)
}

### perform test on all communities produced by one method
community.significance.all <- function(graph, community, silent=FALSE, ...) {
	comms <- levels(as.factor(community$membership))
	result <- NULL
	for(i in comms){
		comm.nodes <- subset(as.matrix(community$names),community$membership==i)
		test.res <- community.significance.test(graph, comm.nodes)
		result <- rbind(result,c(i,test.res$p.value,test.res$statistic,length(comm.nodes)))
	}
	if(!silent){
		cat("\nCommunity\t\tP value\t\t\tW value\n")
		for(i in 1:dim(result)[1]){
			cat(paste(result[i,1],"\t\t\t",result[i,2],"\t\t\t",result[i,3],"\n",sep=""))
		}
		cat("\n")
	}
	return(result)
}

### write p values onto plot
significance.plot <- function(sig,colors){
	if(dim(sig)[1] < 10){
		for(i in 1:dim(sig)[1]){
			text(-1.3,(i*-0.05)+1.2,col=colors[i],labels=c(paste("#",i,": n=",sig[i,4],", p=",round(as.numeric(sig[i,2]),digits=4),sep="")),pos=4)
		}
	}else{
		j <- 1
		for(i in 1:dim(sig)[1]){
				text(-1.3,(j*-0.05)+1.2,col=colors[i],labels=c(paste("#",i,": n=",sig[i,4],", p=",round(as.numeric(sig[i,2]),digits=4),sep="")),pos=4)
				j <- j+1
		}
	}
}
	
### p values and go enrichment on a new plot
significance.plot.new <- function(sig,colors,go=NULL){
	plot.new()
	vsp <- seq(0,1,1/(dim(sig)[1]))
	ifelse(length(go)==0,pcex <- 1, pcex <- 0.8)
	for(i in 1:dim(sig)[1]){
		text(0.5,vsp[i+1],col=colors[i],labels=c(paste("#",i,": n=",sig[i,4],", p=",round(as.numeric(sig[i,2]),digits=4),sep="")),cex=pcex)
		if(length(go)!=0){
			g <- strwrap(paste(go[[i]][,"Term"],collapse=", "),width=120)
			vs <- seq(0,vsp[2],vsp[2]/(length(g)+1))
			j <- 1
			for(gg in g){
				text(0.5,vsp[i+1]-vs[j+1],col=colors[i],labels=gg,cex=0.8)
				j <- j+1
			}
		}
	}	
}

get.members.list <- function(G,c){
	ls <- list()
	for(i in levels(as.factor(c$membership))){
		ls[[i]] <- V(G)$gname[c$membership==i]
	}
	return(ls)
}

### p values and go enrichment on a new plot
plot.members.list <- function(ls,colors){
	# print("plotting names")
	plot.new()
	vsp <- seq(0,1,1/(length(ls)))
	pcex <- 0.8
	for(i in 1:length(ls)){
		text(0.5,vsp[i+1],col=colors[i],labels=c(paste("#",i,sep="")),cex=pcex)
		if(length(ls)!=0){
			g <- strwrap(paste(ls[[i]],collapse=", "),width=100)
			vs <- seq(0,vsp[2],vsp[2]/(length(g)+1))
			j <- 1
			for(gg in g){
				text(0.5,vsp[i+1]-vs[j+1],col=colors[i],labels=gg,cex=0.8)
				j <- j+1
			}
		}
	}
}

#	else{
#		lowp <- as.numeric(sig[,2])<0.3
#		lowp[is.na(lowp)] <- FALSE
#		j <- 1
#		for(i in 1:dim(sig)[1]){
#			if(lowp[i]){
#				text(-1.3,(j*-0.1)+1.3,col=colors[i],labels=c(paste("#",i,": n=",sig[i,4],", p=",round(as.numeric(sig[i,2]),digits=4),sep="")),pos=4)
#				j <- j+1
#			}
#		}
#	}
