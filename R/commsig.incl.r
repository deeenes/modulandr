
### community significance test
community.significance.test <- function(graph, vs, ...) {
    if (is.directed(graph)) stop("This method requires an undirected graph")
    subgraph <- induced.subgraph(graph, vs)
    in.degrees <- degree(subgraph)
    out.degrees <- degree(graph, vs) - in.degrees
    wilcox.test(in.degrees, out.degrees, ...)
}

### perform test on all communities produced by one method
community.significance.all <- function(graph, community, silent=FALSE, ...) {
	comms <- levels(as.factor(community$membership))
	result <- NULL
	for(i in comms){
		comm.nodes <- subset(as.matrix(community$names),community$membership==i)
		test.res <- community.significance.test(graph, comm.nodes, ...)
		result <- rbind(result,c(i,test.res$p.value,test.res$statistic))
	}
	if(!silent){
		cat("\nCommunity\t\tP value\t\t\tW value\n")
		for(i in 1:dim(result)[1]){
			cat(paste(result[i,1],"\t\t\t",result[i,2],"\t\t\t",result[i,3],"\n",sep=""))
		}
		cat("\n")
	}
	return(result)
}

### write p values onto plot
significance.plot <- function(sig,colors){
	if(dim(sig)[1] < 10){
		for(i in 1:dim(sig)[1]){
			text(-1.3,(i*-0.1)+1.3,col=colors[i],labels=c(paste("group #",i,": p=",round(as.numeric(sig[i,2]),digits=4),sep="")),pos=4)
		}
	}else{
		lowp <- as.numeric(sig[,2])<0.3
		j <- 1
		for(i in 1:dim(sig)[1]){
			if(lowp[i]){
				text(-1.3,(j*-0.1)+1.3,col=rbcolors[i],labels=c(paste("group #",i,": p=",round(as.numeric(sig[i,2]),digits=4),sep="")),pos=4)
				j <- j+1
			}
		}
	}
}


