### this is a wrapper for applying any igraph community finding method

# apply an igraph community discovering method
community.generic <- function(G,method,title,ref,col.main="#000000",rec=FALSE,lev=0) {
	# write log
	mname <- paste(G$dir,method,"_",G$input,"_",lev,sep="")
	write(paste("  == ",date()," Starting ",method," method.",sep=""),file=G$logfile,append=TRUE)
	# call community function
	c <- NULL
	if(!file.exists(mname) || G$recalculate){
		# do calculations
		print("community.generic // method")
		print(method)
		if(method=="spinglass"){
			spins <- 10
			c <- do.call(paste(method,".community",sep=""),c(list(G),spins=spins))
		}else{
			c <- tryCatch(do.call(paste(method,".community",sep=""),list(G)),error=function(){save.image(file="last.im")},finally=NULL)
		}
		write.memberships(c,mname)
	}
	c <- read.memberships(G,mname)
	c$algorithm <- method
	# write log
	write(paste("  == ",date()," Finished ",method," method.",sep=""),file=G$logfile,append=TRUE)
	if(G$go && length(G$annot.univ)==0){
		G <- go.read.data(G)
	}
	if(method != "clusters"){
		print("whole graph")
		print(length(V(G)))
		cc <- remove.smalls(G,c)
		if(!is.list(cc)){
			return();
		}
		c <- cc$community
		G <- cc$graph
		print("nodes in enough large nodes")
		print(length(V(G)))
	}
	if(G$go){
		# write log
		write(paste("  == ",date(),
			" Calculating GO enrichment for method ",method,".",sep=""),file=G$logfile,append=TRUE)
		go <- go.enrichment(c,paste(G$input,"_goannot.csv",sep=""))
		# write log
		write(paste("  == ",date(),
			" GO enrichment calculated.",sep=""),file=G$logfile,append=TRUE)
	}
	# plot result
	if(G$plot){
		# if(length(V(G))<200){
		#if(FALSE){
		#	V(G)$label <- V(G)$label
		#}else{
		#	V(G)$label <- NA
		#}
		V(G)$size <- 2
		if(length(G$layout)==0){
			G$layout <- layout.kamada.kawai(G)
		}
		V(G)$color <- rainbow(length(levels(as.factor(c$membership))))[c$membership]
		if(G$go){
			write(paste("  == ",date(),
			" Plotting ",method," communities and GO enrichment.",sep=""),file=G$logfile,append=TRUE)
			plot.community.go(G,c,title,ref,go,col.main)
		}else{
			write(paste("  == ",date(),
			" Plotting ",method," communities.",sep=""),file=G$logfile,append=TRUE)
			plot.community(G,c,title,ref,col.main)
		}
	}
	# write community memberships into file
	if(is.null(G$input)){
		G$input <- rndstr()
	}
	#print("rec")
	#print(rec)
	#print("lev")
	#print(lev)
	if(rec & length(sizes(c)[sizes(c)>50]) > 1){
		lev <- lev+1
		print(paste("inside if, and rec=",rec,sep=""))
		sz <- sizes(c)
		mo <- 1
		for(cc in levels(as.factor(c$membership))){
			if(sz[paste(cc,sep="")] > 50){
				le <- paste(lev,".",mo,sep="")
				mo <- mo+1
				F <- split.module(G,c,cc)
				community.generic(F,method,title,ref,col.main=V(G)$color[c$membership==cc][1],rec=FALSE,lev=le)
			}
		}
	}
}

plot.community.go <- function(G,c,title,ref,go,col.main="#000000"){
	a <- ifelse(G$members,4,3)
	#print("matrix")
	#print(matrix(seq(1,a),ncol=a,byrow=T))
	#print("width")
	#print(c(5,seq(3,by=0,length.out=a-1)))
	if(length(V(G))<=900){
		layout(matrix(seq(1,a),ncol=a,byrow=T),width=c(5,5,seq(3,by=0,length.out=a-2)),height=c(3),respect=FALSE)
	}else{
		pngname  <- paste(fpref,sprintf("%03d",p),".png",sep="")
		pdfname  <- paste(fpref,sprintf("%03d",p),".pdf",sep="")
		p <<- p+1
		png(file=pngname,width=1024,height=768,units="px",type="cairo",bg="white")
	}
	title <- paste(title," :: ",G$input,sep="")
	#print(V(G)$label)
	plot(G)
	title(title,sub=paste("Number of modules:",length(levels(as.factor(c$membership))),":: ",ref),
		col.main=col.main)
	G$layout <- layout.modular(G,c)
	plot(G)
	title(title,sub=paste("Number of modules:",length(levels(as.factor(c$membership))),":: ",ref),
		col.main=col.main)
	if(length(V(G))>900){
		dev.off()
		cairo_pdf(filename=pdfname,width=9,height=9,pointsize=12,family="Futura",onefile=TRUE)
	}
	# print p values
	rbcolors <- rainbow(length(levels(as.factor(c$membership))))
	#print("community.significance.all()")
	sig <- community.significance.all(G,c,silent=TRUE)
	#print("significance.plot.new()")
	significance.plot.new(sig,rbcolors,go)
	if(G$members){
		ls <- get.members.list(G,c)
		# print(ls)
		plot.members.list(ls,rbcolors)
	}
	if(length(V(G))>900){
		dev.off()
	}
}

plot.community <- function(G,c,title,ref,col.main="#000000"){
	plot(G)
	title(title,sub=paste("Number of modules:",length(levels(as.factor(c$membership))),":: ",ref),
		col.main=col.main)
	# print p values
	rbcolors <- rainbow(length(levels(as.factor(c$membership))))
	sig <- community.significance.all(G,c,silent=TRUE)
	significance.plot(sig,rbcolors)
}

# moduland community generic
moduland.generic <- function(G,landscape="linkland",hill="total",pland_x=0.1,col.main="#000000",rec=FALSE,lev=0) {
	write(paste("  == ",date()," Starting moduland ",landscape," ",hill," method.",sep=""),file=G$logfile,append=TRUE)
	fname <- paste(landscape,"_",hill,"_",pland_x,"_",lev,sep="")
	lsfname <- paste(G$dir,landscape,"_",pland_x,"_",lev,sep="")
	hmfname <- paste(lsfname,"_",hill,sep="")
	modfile <- paste(hmfname,".mod",sep="")
	pajek <- paste(lsfname,".net",sep="")
	# call community function
	c <- NULL
	if(!file.exists(modfile) || G$recalculate){
		# do calculations
		write(paste("    »» ",date(),
					" Doing ",landscape," ",hill," calculations, level: ",lev,sep=""), 
					file=G$logfile, append=TRUE)
		F <- G
		F <- remove.edge.attribute(F,"color")
		write.graph(F,format="pajek",file=pajek)
		c <- moduland.community(lsfname,hmfname,G$logfile,landscape,hill,pland_x)
		write.memberships(c,modfile)
	}else{
		# read data from file
		write(paste("    »» ",date(),
			" Reading ",landscape," ",hill," modules data, level: ",lev,sep=""), 
			file=G$logfile, append=TRUE)
		c <- read.memberships(G,modfile)
		c$algorithm <- paste(landscape,".",hill,sep="")
	}
	V(G)$color <- rainbow(length(levels(as.factor(c$membership))))[c$membership]
	# if(length(V(G))<200){
	#if(FALSE){
	#	V(G)$label <- V(G)$name
	#}else{
	#	V(G)$label <- NA
	#}
	V(G)$size <- 2
	lscm <- c("LinkLand","PerturLand","NodeLand","EdgeWeight")
	names(lscm) <- c("linkland","perturland","nodeland","edgeweight")
	hillm <- c("TotalHill","ProportionalHill","GradientHill")
	names(hillm) <- c("total","prop","gradient")
	px <- ifelse(landscape == "perturland", paste(", x=",pland_x,sep=""), "")
	title <- paste(lscm[landscape],px," :: ",hillm[hill],sep="")
	ref <- "Kovács & Csermely, 2010"
	#plot(G,	main=paste(lscm[landscape],px," :: ",hillm[hill],sep=""),
	#	sub=paste("Number of modules:",length(levels(as.factor(c$membership))),":: Kovács & Csermely, 2010"))
	#rbcolors <- rainbow(length(levels(as.factor(c$membership))))
	#sig <- community.significance.all(G,c,silent=TRUE)
	#significance.plot(sig,rbcolors)
	# remove temporary files
	# extcomm.run(paste("rm ",fname,".*",sep=""))
	# write community memberships into file
	if(is.null(G$input)){
		G$input <- rndstr()
	}
	# mname <- paste(G$dir,landscape,"_",hill,"_",rndstr(),"_",G$input,sep="")
	mname <- hmfname
	write.memberships(c,mname)
	# write log
	write(paste("  == ",date()," Finished moduland ",landscape," ",hill," method.",sep=""),file=G$logfile,append=TRUE)
	
	if(G$go & length(G$annot.univ)==0){
		G <- go.read.data(G)
	}
	# return(c)
	cc <- remove.smalls(G,c)
	if(!is.list(cc)){
		return();
	}
	c <- cc$community
	G <- cc$graph
	if(G$go){
		# write log
		write(paste("  == ",date(),
			" Calculating GO enrichment for method ",landscape," ",hill,".",sep=""),file=G$logfile,append=TRUE)
		go <- go.enrichment(c,paste(G$input,"_goannot.csv",sep=""))
		# write log
		write(paste("  == ",date(),
			" GO enrichment calculated.",sep=""),file=G$logfile,append=TRUE)
	}
	# plot result
	print(V(G)$label)
	if(G$plot){
		# if(length(V(G))<200){
		#if(FALSE){
		#	V(G)$label <- V(G)$name
		#}else{
		#	V(G)$label <- NA
		#}
		V(G)$size <- 2
		if(length(G$layout)==0){
			G$layout <- layout.kamada.kawai
		}
		V(G)$color <- rainbow(length(levels(as.factor(c$membership))))[c$membership]
		if(G$go){
			write(paste("  == ",date(),
			" Plotting ",landscape," ",hill," communities and GO enrichment.",sep=""),file=G$logfile,append=TRUE)
			plot.community.go(G,c,title,ref,go,col.main)
		}else{
			write(paste("  == ",date(),
			" Plotting ",landscape," ",hill," communities.",sep=""),file=G$logfile,append=TRUE)
			plot.community(G,c,title,ref,col.main)
		}
	}
	# write community memberships into file
	if(is.null(G$input)){
		G$input <- rndstr()
	}
	if(rec & length(sizes(c)[sizes(c)>50]) > 1){
		lev <- lev+1
		#print(paste("inside if, and rec=",rec,sep=""))
		sz <- sizes(c)
		mo <- 1
		for(cc in levels(as.factor(c$membership))){
			if(sz[paste(cc,sep="")] > 50){
				le <- paste(lev,".",mo,sep="")
				mo <- mo+1
				F <- split.module(G,c,cc)
				moduland.generic(F,landscape=landscape,hill=hill,pland_x=pland_x,col.main=V(G)$color[c$membership==cc][1],rec=FALSE,lev=le)
			}
		}
	}
}


rndstr <- function(lenght=4) {
    randomString <- paste(sample(c(0:9, letters, LETTERS),lenght, replace=TRUE), collapse="")
    return(randomString)
}

write.memberships <- function(c,mname){
	mem <- NULL
	for(i in 1:length(c$names)){
		mem <- rbind(mem,c(c$names[i],c$membership[i]))
	}
	write.table(mem,file=mname,quote=FALSE,col.names=FALSE,row.names=FALSE,sep="\t")
}

read.memberships <- function(G,fname){
	mem <- read.table(fname,sep="\t",header=FALSE)
	if(length(V(G)$name)!=dim(mem)[1]){
		stop("Module file contains unequal number of nodes than target network.")
	}
	c <- n <- m <- NULL
	for(i in V(G)$name){
		for(j in 1:dim(mem)[1]){
			if(i==mem[j,1]){
				m <- c(m,mem[j,2])
				n <- c(n,i)
			}
		}
	}
	c$membership <- m
	c$names <- n
	c$name <- n
	if(length(V(G)$name)!=length(c$membership)){
		stop("Module file contains unequal number of nodes than target network.")
	}
	return(c)
}

comm.members <- function(c,l){
	# extracts a list of members in one cluster
	# c: community memberships, l: id of the community
	m <- NULL
	summary(c)
	for(i in 1:length(c$membership)){
		if(c$membership[i]==l){
			m <- c(m,c$name[i])
		}
	}
	if(length(m)>0){
		return(m)
	}else{
		return(FALSE)
	}
}

go.to.plot <- function(mname){
	
}

remove.smalls <- function(G,c,sm=NULL){
	if(is.null(sm)){
		sm <- round(length(V(G))/20)
	}
	sz <- sizes(c)
	#print("sz")
	#print(sz)
	#print("membership")
	#print(c$membership)
	#print("sm")
	#print(sm)
	#print(length(V(G)))
	#print(length(c$membership))
	#print(V(G))
	#print( sz[c$membership] < sm )
	#print( sz[,c$membership] < sm )
	cc <- list()
	F <- delete.vertices(G,sz[paste(c$membership,sep="")]<sm)
	if(length(V(F))==0){
		return(FALSE)
	}
	# print("c$membership")
	# print(c$membership)
	# print("paste(c$membership,sep='')")
	# print(paste(c$membership,sep=""))
	# print("sz")
	# print(sz)
	# print("sm")
	# print(sm)
	# print("sz[paste(c$membership,sep='')]")
	# print(sz[paste(c$membership,sep="")])
	# print("c$membership[sz[paste(c$membership,sep='')]>=sm]")
	# print(c$membership[sz[paste(c$membership,sep="")]>=sm])
	
	cc$membership <- c$membership[sz[paste(c$membership,sep="")]>=sm]
	cc$names <- c$names[sz[paste(c$membership,sep="")]>=sm]
	p <- 0
	
	# print(cc$membership)
	# save(list=c("G","F","c","cc","sm","sz"),file="rs.im")
	
	for(i in 1:max(cc$membership)){
		if(length(cc$membership[cc$membership==i])==0){
			p <- p+1
		}else if(p>0){
			cc$membership[cc$membership>=i] <- cc$membership[cc$membership>=i]-p
		}
		if(i==max(cc$membership)){
			break
		}
	}
	r <- list()
	r$graph <- F
	r$community <- cc
	return(r)
}

split.module <- function(G,c,n){
	F <- delete.vertices(G,c$membership!=n)
	return(F)
}

clusters.community <- function(G){
	cl <- clusters(G)
	cl$names <- V(G)$name
	return(cl)
}

layout.modular <- function(G,c){
	nm <- length(levels(as.factor(c$membership)))
	gr <- 2
	while(gr^2<nm){
		gr <- gr+1
	}
	i <- j <- 0
	for(cc in levels(as.factor(c$membership))){
		F <- delete.vertices(G,c$membership!=cc)
		F$layout <- layout.kamada.kawai(F)
		F$layout <- layout.norm(F$layout, i,i+0.5,j,j+0.5)
		G$layout[as.numeric(c$membership==cc),] <- F$layout
		if(i==gr){
			i <- 0
			if(j==gr){
				j <- 0
			}else{
				j <- j+1
			}
		}else{
			i <- i+1
		}
	}
	return(G$layout)
}
